%MatLab applications in physics
%Author: Dawid Lazaj
%Technical Physics

%script analyses 'z3_1350V_x20.dat' file

clear;
clc;

myFileName = 'z3_1350V_x20.dat';
myFile = dir(myFileName);
myFileId=fopen(myFileName, 'r');

blockSize = 8589934592; %8gb converted to bytes
arraySize = 8589934592/256; %chopping file
nulls = zeros(1, 255);

for i = 1:fix(myFile.bytes/arraySize) %reading file and splitting to arrays
    data = fread(myFileId, arraySize, 'uint8');
    for j = 1:arraySize
        nulls(data(j)) = nulls(data(j)) + 1;
    end
   disp(i);  %counting to 256 so you can see progress
end

fclose(myFileId);

noise = NoiseLevel(nulls); %require script of NoiseLevel function

%myFileId = fopen(myFileName, 'r');
%pulses = CountPulses(data);    %require script of CountPulses function
%pulseUncertainty = round(1/sqrt(pulses), 2, 'significant');
%fclose(myFileName);

fprintf('Noise level: %d \n', noise);
%fprintf('Number of pulses: %d \n', pulses);

%plotting histogram
Plot = plot(nulls);
xlabel('Samples');
ylabel('Level');
title('Histogram');

%saving results in file
myFileName = fopen('z3_1350V_x20_peak_analysis_results.dat ','w');
fprintf(myFileName, 'Noise level: %d\n', noise);
%fprintf(myFileName, 'Number of pulses: %d\n', pulses);
%fprintf(myFileName, 'Pulse uncertainty: %.4f\n', pulseUncertainty);
fclose(myFileName);


