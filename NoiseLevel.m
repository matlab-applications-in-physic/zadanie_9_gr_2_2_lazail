%MatLab applications in physics
%Author: Dawid Lazaj
%Technical Physics

function NL = NoiseLevel(data)
    [index] = find(data == max(data));
    NL = index;
end